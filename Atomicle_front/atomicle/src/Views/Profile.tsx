import '../css/Profile.css';
import '../css/style.css';

import {useState, useEffect} from 'react';
import {Button} from 'react-bootstrap';

import ChangeProfileModal from '../components/ChangeProfileModal';

const Profile = (props: {userId: string, name: string, firstName: string, secondName: string, thirdName: string, birthDate: string, country: string, avatarName: string, setAvatarName: (avatarName : string) => void}) => {

    const [avatarPopupVisibility, setAvatarPopupVisibility] = useState(false);

    let fullUserName = props.firstName;
    let currentProfileImage = process.env.REACT_APP_PROFILE_IMAGES + props.avatarName;

    if (props.secondName !== '') fullUserName += " " + props.secondName;
    if (props.thirdName !== '') fullUserName += " " + props.thirdName;
    if (props.name !== '') fullUserName += " " + props.name;

    const changeAvatarPopupClose = () => setAvatarPopupVisibility(false);

    return(
        <>
            <div className='profile-container'>
                <header>
                <div>
                    <h1>{fullUserName}</h1>
                    <p>
                        
                    </p>
                </div>
            </header>
            <section className="profile-content">
                <div>
                <h2></h2>
                <p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Id vero hic ipsa quos doloribus, illo optio sit obcaecati sapiente odit non deleniti quibusdam, rem molestiae dicta tempore voluptate, unde veniam reiciendis rerum ducimus quod culpa. Omnis enim numquam officiis ipsam pariatur officia totam explicabo laboriosam, voluptas nobis veritatis unde fugiat!</p>
                </div>
                <div className="center">
                <img src={currentProfileImage} alt="Not found" className="profile-image"></img>
                <Button variant="primary" onClick={() => setAvatarPopupVisibility(true)}>Click me</Button>
                <ChangeProfileModal userId={props.userId} show={avatarPopupVisibility} onHide={changeAvatarPopupClose} updateAvatar={props.setAvatarName}></ChangeProfileModal>
                </div>
                <div>
                <p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Id vero hic ipsa quos doloribus, illo optio sit obcaecati sapiente odit non deleniti quibusdam, rem molestiae dicta tempore voluptate, unde veniam reiciendis rerum ducimus quod culpa. Omnis enim numquam officiis ipsam pariatur officia totam explicabo laboriosam, voluptas nobis veritatis unde fugiat!</p>
                </div>
                <div>
                4
                </div>

            </section>
            </div>
        </>
    );
};

export default Profile;
import { SyntheticEvent, useState } from 'react';
import { Redirect } from 'react-router';

import '../css/Register.css';
import logo from '../images/logo.svg';
import { Button, Form } from 'react-bootstrap';
import { CountryDropdown } from 'react-country-region-selector';

const Register = () => {

    const [firstName,setFirstName] = useState('');
    const [secondName,setSecondName] = useState('');
    const [thirdName,setThirdName] = useState('');
    const [name,setLastName] = useState('');
    const [birthDate,setBirthDate] = useState('');
    const [country,setCountry] = useState('');
    const [email,setEmail] = useState('');
    const [password,setPassword] = useState('');
    const [dateCreated,setDateCreated] = useState('');

    const emailRegExp = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

    const passwordRegExp = new RegExp("^(((?=.*[a-z])(?=.*[A-Z]))|((?=.*[a-z])(?=.*[0-9]))|((?=.*[A-Z])(?=.*[0-9])))(?=.{8,20})");

    const [redirect, setRedirect] = useState(false);

    function validateName() {return name.length !== 0;};
    function validateFirstName() {return firstName.length !== 0;};
    function validateDate()
    {
        var dateofbirth = new Date(birthDate).getTime();
        var now = new Date().getTime();
        var timeDifferenceInYears = (now - dateofbirth) / (1000 * 60 * 60 * 24 * 365);

        return timeDifferenceInYears > 16;
    }
    function validateCountry() {return country !== 'Select Country' && country !== '';};
    function validateEmail() { return emailRegExp.test(email);};
    function validatePassword()  { return passwordRegExp.test(password);};

    function validateForm()
    {
        if (!validateName())
        {
            alert('Field \"Last name\" cannot be blank.');
            return false;
        }
        if (!validateFirstName())
        {
            alert('Field \"First Name\" cannot be blank.');
            return false;
        }
        if (!validateDate())
        {
            alert('As a user, you must be at least 16 years old.');
            return false;
        }
        if (!validateCountry())
        {
            alert('Select valid country from the list.');
            return false;
        }
        if (!validateEmail())
        {
            alert('Invalid email format.');
            return false;
        }
        if (!validatePassword())
        {
            alert('Invalid password. Password needs to have min 8 characters, max 20 characters, at least one capital letter, at least one small letter, at least one number and at least one special character.');
            return false;
        }
        return true;
    }

    const submit = async (e: SyntheticEvent) => 
    {
        e.preventDefault();

        if (validateForm())
        {
            setDateCreated(new Date().toISOString().slice(0, 23));

            await fetch(process.env.REACT_APP_API + 'register',
            {
                method: 'POST',
                headers: {'Content-Type': 'application/json'},
                body: JSON.stringify(
                    {
                        name,
                        firstName,
                        secondName,
                        thirdName,
                        birthDate,
                        country,
                        email,
                        password,
                        dateCreated
                    }
                )
            });

            setRedirect(true);
        }
    }

    if (redirect) return <Redirect to="/login"/>;

    return (
        <>
            <div className="register-container">

                <div className="register-form-container">
                    <div className="register-data">

                        <h1>Create new account</h1>

                        <Form className="register-form" onSubmit={submit}>

                            <Form.Group controlId="LastName">
                                <Form.Label>Last name</Form.Label>
                                <Form.Control type="text" name="LastName" placeholder="Smith" onChange={e => setLastName(e.target.value)} />
                            </Form.Group>
                            <Form.Group controlId="FirstName">
                                <Form.Label>First name</Form.Label>
                                <Form.Control type="text" name="FirstName" placeholder="John" onChange={e => setFirstName(e.target.value)} />
                            </Form.Group>
                            <Form.Group controlId="SecondName">
                                <Form.Label>Second name</Form.Label>
                                <Form.Control type="text" name="SecondName" placeholder="George" onChange={e => setSecondName(e.target.value)} />
                            </Form.Group>
                            <Form.Group controlId="ThirdName">
                                <Form.Label>Third name</Form.Label>
                                <Form.Control type="text" name="ThirdName" placeholder="Matthew" onChange={e => setThirdName(e.target.value)} />
                            </Form.Group>
                            <Form.Group controlId="BirthDate">
                                <Form.Label>Date of birth</Form.Label>
                                <Form.Control type="date" name="BirthDate" onChange={e => setBirthDate(e.target.value)} />
                            </Form.Group>
                            <Form.Group controlId="Country">
                                <Form.Label>Country</Form.Label>
                                <CountryDropdown name="Country" value={country} onChange={e => setCountry(e)} />
                            </Form.Group>
                            <Form.Group controlId="Email">
                                <Form.Label>Email</Form.Label>
                                <Form.Control type="email" name="Email" placeholder="example@domain.com" onChange={e => setEmail(e.target.value)} />
                            </Form.Group>
                            <Form.Group controlId="Password">
                                <Form.Label>Password</Form.Label>
                                <Form.Control type="password" name="Password" placeholder="At least 10 characters" onChange={e => setPassword(e.target.value)} />
                            </Form.Group>
                            <Form.Group>
                            </Form.Group>
                            <Form.Group>
                                <Button variant="primary" type="submit" name="Submit">Create account</Button>
                            </Form.Group>
                        </Form>
                    </div>
                </div>

                <div className="register-logo">
                    <img src={logo} alt="Atomicle logo"></img>
                </div>
            </div>
        </>
    );
};

export default Register;

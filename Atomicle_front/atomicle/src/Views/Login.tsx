import { SyntheticEvent, useState } from 'react';
import { Link } from 'react-router-dom';

import '../css/Login.css';
import logo from '../images/logo.svg';
import { Button, Form } from 'react-bootstrap';

const Login = (props: {setUserId: (userId: string) => void}) => {

    const [email,setEmail] = useState('');
    const [password,setPassword] = useState('');
    const [redirect,setRedirect] = useState(false);

    let a = '';
    const submit = async (e: SyntheticEvent) => 
    {
        e.preventDefault();
        
        const response = await fetch(process.env.REACT_APP_API + 'login',
        {
            method: 'POST',
            headers: {'Content-Type': 'application/json'},
            credentials: 'include',
            body: JSON.stringify(
                {
                    email,
                    password
                }
            )
        });

        const content = await response.json();
        a = content.userId;
        
        setRedirect(true);
    };

    if (redirect)
    {
        props.setUserId(a);
    }

    return (
        <>
            <div className="login-container">

                <div className="login-form-container">

                    <div className="login-data">

                        <Form className="login-form" onSubmit={submit}>

                            <h1>Login to your account</h1>

                            <Form.Group controlId="Email">
                                <Form.Label>Email</Form.Label>
                                <Form.Control type="email" name="Email" placeholder="example@domain.com" required onChange={e => setEmail(e.target.value)} />
                            </Form.Group>
                            <Form.Group controlId="Password">
                                <Form.Label>Password</Form.Label>
                                <Form.Control type="password" name="Password" required onChange={e => setPassword(e.target.value)} />
                            </Form.Group>
                            <Form.Group>
                                <Button variant="primary" type="submit" name="Submit">Login</Button>
                            </Form.Group>
                        </Form>
                        
                        <Link to="/register" className="create-account-link">Don't have an account? Create one!</Link>
                    </div>
                </div>
                <div className="logo">
                    <img src={logo} alt="Atomicle logo"></img>
                </div>
            </div>
        </>
    );

};

export default Login;


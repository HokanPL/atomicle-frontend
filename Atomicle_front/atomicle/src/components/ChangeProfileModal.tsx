import { SyntheticEvent } from 'react';
import {Modal, Button, Row, Col, Form} from 'react-bootstrap';


//no idea how to sent integer in props, so 'any' workaround
const ChangeProfileModal = (props: {userId: string, show: boolean, onHide: () => void, updateAvatar: (avatarName: string) => void}) =>
{
    
    let filename = "default.jpg";  
    let avatar;

    const handleFileSelected = (e) => 
    {
        e.preventDefault();
        filename = e.target.files[0].name;
        avatar = e.target.files[0];

        console.log(avatar.name);
        console.log(filename);
    }

    const submit = async (e: SyntheticEvent) => 
    {
        e.preventDefault();

        let id = props.userId;
        let formData = new FormData();
        formData.append(
            "myFile",
            avatar, filename
        );

        await fetch(process.env.REACT_APP_API + 'uploadavatar',
        {
            method: 'POST',
            body: formData
        });

        await fetch(process.env.REACT_APP_API + 'updateuseravatar',
        {
            method: 'PUT',
            headers: {'Content-Type': 'application/json'},
            body: JSON.stringify(
                {
                    id,
                    filename
                }
            )
        });

        props.onHide();
        props.updateAvatar(filename);
    };

    return(
        <div className="container">
            <Modal
            {...props}
            size="lg"
            aria-labelledby="contained-modal-title-vcenter"
            centered>

                <Modal.Header>
                    <Modal.Title id="contained-modal-title-vcenter">
                        Update your profile image
                    </Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <Form onSubmit={submit}>
                        <Row>
                            <Form.Group>
                                <Form.File name="uploadedAvatar" accept="image/png, image/gif, image/jpeg" onChange={handleFileSelected}>
                                    
                                </Form.File>
                            </Form.Group>
                        </Row>
                        <Row>
                            <Col>
                                <Form.Group>
                                    <Button variant="primary" type="submit">
                                        Change avatar
                                    </Button>
                                </Form.Group>
                            </Col>
                            <Col>
                                <Form.Group>
                                    <Button variant="danger" onClick={props.onHide}>Close</Button>
                                </Form.Group>
                            </Col>
                        </Row>
                    </Form>
                </Modal.Body>
            </Modal>
            



        </div>
    );

};

export default ChangeProfileModal;

import './css/App.css';
import './css/form.css';

import Home from './views/Home';
import Login from './views/Login';
import Register from './views/Register';
import Profile from './views/Profile';
import ViewMock from './views/ViewMock';
import Navbar from './components/Navbar';

import {useEffect, useState} from 'react';
import {BrowserRouter as Router, Switch, Route} from 'react-router-dom';

function App()
{
  const [userId, setUserId] = useState('');
  const [firstName, setFirstName] = useState('');
  const [name, setName] = useState('');
  const [secondName, setSecondName] = useState('');
  const [thirdName, setThirdName] = useState('');
  const [birthDate, setBirthDate] = useState('');
  const [country, setCountry] = useState('');
  const [avatarName, setAvatarName] = useState("default.jpg");

  useEffect(() => {
      (
          async () => {
              const response = await fetch(process.env.REACT_APP_API + 'user',
              {
                  method: 'GET',
                  headers: {'Content-Type': 'application/json'},
                  credentials: 'include'
              });

              const content = await response.json();

              setUserId(content.userID);
              setName(content.name);
              setFirstName(content.firstName);
              setSecondName(content.secondName);
              setThirdName(content.thirdName);
              setBirthDate(content.birthDate);
              setCountry(content.country);
              setAvatarName(content.avatarName);
          }
      )();
  });

  if (typeof userId === 'undefined' || userId === ''){
    
    return (
      <div className="root-container">
        <Router>
          <Switch>
              <Route path='/register' component={() => <Register/>} />
              <Route path='/login' component={() => <Login setUserId={setUserId}/>} />
              <Route path='/' component={() => <Login setUserId={setUserId}/>} />
              
          </Switch>
        </Router>
      </div>
    );
    }
    else{
    return (
      <div className="root-container">
        <Router>
          
          <Switch>
              <Route path='/account' component={() => <><Navbar userId={userId} setUserId={setUserId}/><Profile userId={userId} name={name} firstName={firstName} secondName={secondName} thirdName={thirdName} birthDate={birthDate} country={country} avatarName={avatarName} setAvatarName={setAvatarName}/></>} />
              <Route path='/resources' component={() => <><Navbar userId={userId} setUserId={setUserId}/><ViewMock /></>} />
              <Route path='/research' component={() => <><Navbar userId={userId} setUserId={setUserId}/><ViewMock /></>} />
              <Route path='/articles' component={() => <><Navbar userId={userId} setUserId={setUserId}/><ViewMock /></>} />
              <Route path='/forum' component={() => <><Navbar userId={userId} setUserId={setUserId}/><ViewMock /></>} />
              <Route path='/community' component={() => <><Navbar userId={userId} setUserId={setUserId}/><ViewMock /></>} />
              <Route path='/' component={() => <><Navbar userId={userId} setUserId={setUserId}/><Home userId={userId}/></>} />
          </Switch>
        </Router>
      </div>
    );}
}

export default App;
